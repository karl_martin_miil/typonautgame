var gulp = require('gulp');
var spawn = require('child_process').spawn;
var browserSync = require('browser-sync').create();
// Define base folders
var src = 'src/';
var dest = 'build/';
var node;


gulp.task('js', function() {
    return gulp.src(src + '**/*.js').pipe(gulp.dest('build'))
});

gulp.task('sound', function() {
    return gulp.src(src + '**/*.wav').pipe(gulp.dest('build'))
});

gulp.task('css', function() {
    return gulp.src(src + '**/*.css').pipe(gulp.dest('build'))
});

gulp.task('html', function () {
    return gulp.src(src + '**/*.html').pipe(gulp.dest(dest));
});

gulp.task('browser-reload', function () {
    browserSync.reload();
});

gulp.task('server', function() {
    if (node) node.kill()
    node = spawn('node', ['build/index.js'], {stdio: 'inherit'})
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
})

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./build/public"
        }
    });
});

//When doing front end stuff and you don't want to test the functionality of socket io uncomment these for live reload
/*
gulp.task('run', ['html', 'js', 'watch', 'browser-sync', 'server']);

gulp.task('watch', function () {
    gulp.watch('src/!**!/!*.js', ['js', 'server', 'browser-reload']);
    gulp.watch('src/!**!/!*.html', ['html', 'server', 'browser-reload']);
});
*/

//When doing front end stuff and you don't want to test the functionality of socket io comment these for live reload
gulp.task('run', ['html', 'js', 'watch', 'server', 'css', 'sound']);

gulp.task('watch', function () {
    gulp.watch('src/**/*.js', ['js', 'server']);
    gulp.watch('src/**/*.html', ['html', 'server']);
    gulp.watch('src/**/*.css', ['css']);
});

process.on('exit', function() {
    if (node) node.kill()
})