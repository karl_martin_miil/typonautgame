var randomWords = require('random-words');

function GameService(lobby, io) {
    this.lobby = lobby;
    this.io = io;
    this.time = 0;
    this.points = 10;
    this.broadcastToLobby("lobby", lobby)
}
GameService.prototype = {
    sendWords: function () {
        this.word = randomWords();
        this.points = this.word.length;
        this.lobby.addWord(this.word);
        this.broadcastToLobby("word", this.word);
        this.time = 0;
    },
	startGame: function () {
		this.sendWords();
        this.startWordTimer();
    },
	checkAnswerAndGiveFeedback: function (data, playerId) {
		if (data == this.word) {
			console.log("correct word");
			console.log(this.time);
            var points = this.points-this.time;
            if (points < 0) {
                points = 0;
            }
            this.lobby.addPoints(this.word, playerId, points);


            //this is for when we want to update the leaderboard when the second player has gotten it right aswell
            if (this.lobby.bothHavePoints(this.word)) {
                this.broadcastToLobby("feedback", {both: true, correct: true, word: this.word, scoreBoard: this.lobby.scoreBoard(),
                    players: {playerOne: this.lobby.playerOne, playerTwo: this.lobby.playerTwo}})
                //if this is sent out, show the leaderboard for 3 seconds and generate new words for both.
                var that = this;
                setTimeout(function(){that.sendWords() }, 3000);
            }else {
                this.io.to(playerId).emit("feedback", {both: false, correct: true, word: this.word, scoreBoard: this.lobby.scoreBoard(),
                    players: {playerOne: this.lobby.playerOne, playerTwo: this.lobby.playerTwo}})
            }
		} else {
			console.log("wrong word");
			this.io.to(playerId).emit("feedback", {correct: false, word: this.word})
		}
	},
	broadcastToLobby: function (key, value) {
		this.io.to(this.lobby.playerTwo.id).emit(key, value);
		this.io.to(this.lobby.playerOne.id).emit(key, value);
	},
    startWordTimer: function() {
        var that = this;
        setInterval(function(){
            that.time = that.time + 1;
        }, 1000);
	},
	destroy: function() {
		//maybe show score table data here
         this.broadcastToLobby("delete", "Your opponent has left, looking for a new player");
	}
};

module.exports = GameService;