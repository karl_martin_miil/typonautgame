var socket = io.connect('http://localhost:3002');
var $lobbyInfo = $("#lobbyInfo"),
	$word = $('.word'),
	$loaderArea = $('.loader-area'),
	$wordLabel = $('#label-for-word'),
	$gameTable = $('.game-table'),
	scores = [];

$("#nameForm input:first").focus();


//CC
//https://www.freesound.org/people/NenadSimic/sounds/171756/
var point = document.createElement('audio');
point.setAttribute('src', 'sounds/add2.wav');
//CC
//https://www.freesound.org/people/distillerystudio/sounds/327738/
var err = document.createElement('audio');
err.setAttribute('src', 'sounds/err.wav');


socket.on("lobby", function (data) {
	// console.log(data);
	if (data.playerTwo === null) {
		$word.html('<div class="loader"></div>');
	} else {
		if (data.playerOne.name) {
            $wordLabel.html("New lobby found, Waiting for other player.");
        }else {
            $wordLabel.html("Lobby found, please insert your name");
        }
        $word.html('<div class="loader"></div>');
	}

	//$wordLabel.html('Waiting for other player.');
});

socket.on("word", function (data) {
	$word.text(data);
	//$loaderArea.html('<span class="glyphicon glyphicon-play ready-to-go"></span>');
	$loaderArea.html('');
    //$wordLabel.html('You\'ve got to type the word!');
	$wordLabel.html('Type the word!');
    showWordHideLeaderBoard();
});

function hideWordShowLeaderBoard() {
	$(".word-logic-area").hide(300, function() {
        $(".scoreboard").show("drop", {}, 1000, function() {
            RestartConfetti();
		});
	});
}

function showWordHideLeaderBoard() {
    $(".scoreboard").hide("drop", {}, 1000, function () {
        $(".word-logic-area").show(500, function() {
            $("#wordForm input:first").focus();
		});
    });
}


socket.on("feedback", function (data) {
    if (data.correct === false) {
        err.play();
        $word.html('<span class="glyphicon glyphicon-remove ready-to-go"></span>');
        $wordLabel.html("come on you can do it!");
        setTimeout(function () {
            $word.text(data.word);
            $wordLabel.html('Type the word!');
        }, 500);
    }  else {
		if (!data.both) {
            point.play();
            StartConfetti()
        }

        $word.html('<span class="glyphicon glyphicon-ok ready-to-go"></span>');
		setTimeout(function() {
            hideWordShowLeaderBoard();
		}, 500)

		// very bad code, but what can you do
		var playerOneName = data.players.playerOne.name,
			playerTwoName = data.players.playerTwo.name;

        var scoreboard = "";
        var playerOneSum = 0, playerTwoSum = 0


		$.each(data.scoreBoard, function(i, score) {
			var winner = "- DRAW -";
			if (score.playerOne > score.playerTwo) {
                winner = playerOneName;
			} else if (score.playerOne < score.playerTwo) {
                winner = playerTwoName;
            }
			scoreboard += '<tr>' +
			'<th>' + i + '</th>'+
			'<th>' + score.word + '</th>'+
			'<th>' + playerOneName + '</th>'+
			'<th>' + playerTwoName + '</th>'+
			'<th>' + winner + '</th>'+
			'<th>' + score.playerOne + '</th>'+
			'<th>' + score.playerTwo + '</th>'+
			'</tr>'
            $("#gameInfo").html(scoreboard);
			if(score.playerOne!=null){
                playerOneSum += score.playerOne
            }
            if(score.playerTwo!=null) {
                playerTwoSum += score.playerTwo
            }

            $(".player-scores").html("<h2>"+playerOneName+":<span>"+playerOneSum+"</span> "+playerTwoName+":<span>"+playerTwoSum+"</span></h2>");

        });

		$gameTable.append('</table>');
		// end of badness
		// $lobbyInfo.html("correct word, " + JSON.stringify(data.scoreBoard) + JSON.stringify(data.players));
		console.log( "scoreboard> ")
		console.log( data.scoreBoard)
		console.log( "players> ")
		console.log( data.players)
		$("#wordForm input:first").val("");
	}
});

$("#wordForm").submit(function (event) {
	event.preventDefault();
	var word = $("#wordForm input:first").val();
	$(this).attr('action', "http://localhost:3002");
	socket.emit("answer", word.toLowerCase());
});

$("#nameForm").submit(function (event) {
	event.preventDefault();
	$(this).attr('action', "http://localhost:3002");
	socket.emit("name", $("#nameForm input:first").val());
	$('.name-div').hide();
	$('.added-name').text('You can do it, ' + $("#nameForm input:first").val());
	$('.main-row').show();
    $wordLabel.html("Hello <b>"+ $("#nameForm input:first").val() +"</b> waiting for other player");
});

socket.on("delete", function (data) {
	$lobbyInfo.html(data);
	$word.html("<small>The word will appear here.</small>");
    $word.html('<div class="loader"></div>');
	socket.emit("newLobby", "");
    showWordHideLeaderBoard();
});
