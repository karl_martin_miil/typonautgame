var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	io = require('socket.io')(http),
	lobbyService = require('./lobby/lobby-service'),
	GameService = require('./game/game-service'),
	path = require('path'),
	Player = require('./lobby/player');


app.use("/", express.static(path.join(__dirname, '/public')));

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/public/index.html');
});

app.get('/*', function (req, res) {
	res.sendFile(__dirname + '/public/error-redirect.html');
});

io.on('connection', function (socket) {
	var player = new Player(socket.id);
	var lobby = lobbyService.add(player);
	socket.emit("lobby", lobby);

	if (lobbyService.checkForPlayers(lobby)) {
		makeGame()
	}

	function makeGame() {
		lobby.gameService = new GameService(lobby, io);
		if (lobbyService.checkForNames(lobby)) {
			lobby.gameService.startGame();
		}
	}

	socket.on("answer", function (data) {
		if (lobbyService.checkForPlayers(lobby)) {
			lobby.gameService.checkAnswerAndGiveFeedback(data, socket.id);
		}
	});

	socket.on("name", function (data) {
		player.setName(data);
		if (lobbyService.checkForNames(lobby)) {
			lobby.gameService.startGame();
		}
	});

	socket.on('disconnect', function () {
		console.log('user disconnected');
		lobbyService.destroy(lobby);
	});

	socket.on('newLobby', function () {
		lobby = lobbyService.add(player);
		if (lobbyService.checkForPlayers(lobby)) {
			makeGame()
		}
	});
});


http.listen(3002, function () {
	console.log('listening on *:3002');
});