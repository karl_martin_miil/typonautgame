var uuid = require('uuid/v1');
var HashMap = require('hashmap');


function Lobby() {
    this.playerOne = null;
    this.playerTwo = null;
    this.id = uuid();
    this.gameService = null;
    this.words = new HashMap;

}
Lobby.prototype = {
    addPlayer: function (player) {
        console.log("added player " + player.id);
        if (this.playerOne === null) {
            this.playerOne = player;
        } else if (this.playerTwo === null) {
            this.playerTwo = player;
        } else {
            throw new Error("no player slots");
        }
    },
    addWord: function (word) {
        this.words.set(
            word, {
                word: word,
                playerOne: null,
                playerTwo: null
            }
        )
    },
    addPoints: function (word, playerId, points) {
        var leaderWord = this.words.get(word);
        if (playerId == this.playerOne.id) {
            leaderWord.playerOne = points;
            this.words.set(word, leaderWord);
        }else {
            leaderWord.playerTwo = points;
            this.words.set(word, leaderWord);
        }
    },
    scoreBoard: function () {
        return this.words.values();
    },
    bothHavePoints: function(word) {
        var leaderWord = this.words.get(word);
        if (leaderWord.playerOne != null && leaderWord.playerTwo != null) {
            return true;
        }
        return false;
    }
};

module.exports = Lobby;