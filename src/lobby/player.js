function Player(playerId) {
	console.log("making a new player object " + playerId);
	this.id = playerId;
	this.name = null;
}

Player.prototype = {
	setName: function (name) {
		this.name = name;
		console.log(this.id + ' got a name: ' + name);
	}
};

module.exports = Player;