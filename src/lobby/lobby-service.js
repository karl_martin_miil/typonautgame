var Lobby = require('./lobby');
var HashMap = require('hashmap');

var lobbies = new HashMap();
var service = {};

service.add = function (player) {
	var lobby = getLobby();
	lobby.addPlayer(player);
	lobbies.set(lobby.id, lobby);
	return lobby;
};

function findEmptyLobby() {
	return lobbies.values().find(function (lobby) {
		return lobby.playerTwo === null;
	});
}

function getLobby() {
	var lobby = findEmptyLobby();
	if (lobby === undefined) {
		lobby = new Lobby();
	}
	console.log("got a lobby " + lobby.id);
	return lobby;
}

service.checkForPlayers = function(lobby) {
    if (lobby.playerTwo !== null && lobby.playerOne !== null) {
        return true
    }
    return false;
}

service.destroy = function (lobby) {
	lobbies.remove(lobby.id);
    if (lobby.gameService) {
        lobby.gameService.destroy();
    }
};

service.checkForNames = function(lobby) {
    if (lobby.playerTwo !== null && lobby.playerOne !== null) {
		if (lobby.playerTwo.name !== null && lobby.playerOne.name !== null) {
			return true;
		}
    }
    return false;
}

module.exports = service;
