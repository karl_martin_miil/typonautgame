**install:**   
```
npm install
```
    
**run:**
```
gulp run
```
this will watch files and build a minified version of the app to build folder

**deployment:**
copy all contents from build folder to server and run:
```
node index.min.js
```
